import Home from "./src/Home";
import Componet from "./src/Component";
import Login from "./src/login";


import {enableScreens} from "react-native-screens";
import React from 'react';

enableScreens()

import {NavigationContainer} from "@react-navigation/native";
import {createNativeStackNavigator} from "react-native-screens/native-stack";
import ProfileScreen from "./src/Profile";


const Stack = createNativeStackNavigator()

export default function App(){
return (
<NavigationContainer>
<Stack.Navigator >

<Stack.Screen name='Home'component={Home} />
<Stack.Screen name='Componet' component={Componet}/>
<Stack.Screen name='ProfileScreen' component={ProfileScreen}/>
<Stack.Screen name='Login'component={Login} />
</Stack.Navigator>
</NavigationContainer>
)
}
