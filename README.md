


Q1) What platforms are available for developing mobile apps?

ANSWER:- 
Basically there is a four platforms are available for developing mobile apps are
1) Native app
native contain seprate tools for ios and android app development 
ios:- Xcode,Appcode,Atom
android :- Android studio, AndroidIDE, Intellij IDEA

2) Hybrid app 
tools for hybrid are same for both android and ios like ionic. apache cordova ,vs

3) Cross-platform 
tool for cross-platforms are also same for both android and ios as the word says "cross-platform" like react native, xamarin,flutter

4) Progressive web app
tools for this are angular,react,polymer.

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Q2) Native(Java/Kotlin/Swift/Objective-c) vs Cross Platforms(React-native, Flutter, Cordova, ionic) 

ANSWER:-
Native apps are developed in platform specific language. For instance, Objective C and Swift for iOS, while Java or Kotlin for Android.
No multi-platform support
High development costs if different OS support is needed
No code reuse
have diffrent code base



 Cross-platform apps are developed in Javascript which is compatible with multiple platforms. 
 used for multi-platform support
low development costs if different OS support is needed
code are reuseable.
this is fatser then native to release
comman code base for ios and android
fixing bugs on both platform once

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Q3) Core differences among - React-native, Flutter, Cordova, ionic etc

ANSWER:-
React-native:React Native uses React library and JavaScript to deliver a native experience on iOS and Android. 
React Native lets you build an application that provides native look-and-feel by invoking native APIs and components.
Native UI components and features like Hot Reloading make it one of the best cross-platform app development tools available in the market. One of the architectural advantages of React Native app development is JavaScript. React Native offers native capabilities through React and JavaScript to build better apps

Flutter:-Flutter is a bit different here as it uses Dart language which is developed by Google. Dart is a modern, multi-paradigm, and objected-oriented programming language used for building apps for web, mobile, and desktop apps.
Flutter uses Dart which is still new and requires learning Dart to create an application. It is gradually improving and has many to-dos to accomplish.High performance and graphicaly enhane app.

ionic:-Ionic makes the best use of web technologies like HTML, CSS, and JavaScript and requires a Cordova plugin to wrap the applications in native containers.ionic has interactive and faster app performence wise.
One codebase, any platform, this gives developers a significant benefit of code reusability. 


Cordova is a very powerful tool for a JavaScript developer, since by simply developing in JavaScript one becomes a general developer of software and applications by implementing web technologies for various environments.
Cordova is not just an HTML application that runs in a browser, it allows you to write native plugins that work with any of the supported platforms, and a JavaScript container will integrate your HTML application with the native code.
Cordova does an amazing job in the field of hybrid application is super easy to use and functional.

I
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Q4)Why react native and why not? (Pros and Cons)
ANSWER:-
it is based on cross-platform where RN can develop mobile application for both android and ios . it is open source and huge community support . used to build a faster and interactive UI app.
Pros
Different OS support

UI performance is almost as fast as native

Code reuse

Cost-effective development


cons
Slower performance

Limited access to OS features
(however, not as limited as for hybrid)

Poor interaction with other native apps

Q5) Execution process of react native code.

Generally, we can separate React Native into 3 parts :

1) React Native – Native side

2) React Native – JS side

3) React Native – Bridge


1) React Native – Native side
Native Code/Modules: Most of the native code in case of iOS is written in Objective C or Swift, while in the case of Android it is written in Java or Kotlin. But for writing our React Native app, we would hardly ever need to write native code for iOS or Android.

2) React Native – JS side
Javascript VM: The JS Virtual Machine that runs all our JavaScript code. On iOS/Android simulators and devices React Native uses JavaScriptCore, which is the JavaScript engine that powers Safari. JavaScriptCore is an open source JavaScript engine originally built for WebKit. In case of iOS, React Native uses the JavaScriptCore provided by the iOS platform. 

3) React Native – Bridge
 React Native bridge is a C++/Java bridge which is responsible for communication between the native and Javascript thread. A custom protocol is used for message passing.
